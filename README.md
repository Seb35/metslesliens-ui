# metslesliens-ui

Cette interface utilisateur permet de visualiser les liens dans un texte de loi français et de debuguer la grammaire permettant leur reconnaissance. La grammaire en elle-même est issue du projet [metslesliens-js](https://framagit.org/parlement-ouvert/metslesliens/tree/master-js), portage en JavaScript du projet de même nom en Python.

Pour démonstration, cette interface est disponible sur https://archeo-lex.fr/metslesliens/.

# English

This user interface aims at visualising the links in a French law text, as well as debugging the associated grammar used for their parsing. The grammar comes itself from the project [metslesliens-js](https://framagit.org/parlement-ouvert/metslesliens/tree/master-js), port in JavaScript of the project with the same name in Python.

For a demonstration, you can visit https://archeo-lex.fr/metslesliens/.

# Installation

```
git clone https://framagit.org/Seb35/metslesliens-ui
cd metslesliens-ui
npm install
npx sapper dev # development
npm run dev:workers # development
npm run build && node __sapper__/build # production
```

# Licence

Licence WTFPL 2.0
