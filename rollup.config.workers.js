import resolve from 'rollup-plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { terser } from 'rollup-plugin-terser';

const mode = process.env.NODE_ENV;
const dev = mode === 'development' || process.env.ROLLUP_WATCH;

const onwarn = (warning, onwarn) => {
  if (warning.code === 'EVAL' && warning.loc && warning.loc.file && warning.loc.file.indexOf('/node_modules/pegjs/') !== -1) return;
  return onwarn(warning);
}

// bundle workers
export default ['webworker-grammar'].map(x => ({
	input: `src/workers/${x}.js`,
	output: [
		{
			file: `static/workers/${x}.js`,
			format: 'iife'
		}
	],
	plugins: [
		resolve({
			browser: true
		}),
		commonjs(),
		!dev && terser()
	],
	onwarn
}));
