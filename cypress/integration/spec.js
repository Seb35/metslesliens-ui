describe('Sapper template app', () => {
	beforeEach(() => {
		cy.visit('/')
	});

	it('navigates to /aide', () => {
		cy.get('nav a').contains('aide').click();
		cy.url().should('include', '/aide');
	});
});
