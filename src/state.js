import { writable } from "svelte/store";

export const showGrammar = writable( true );
export const showText = writable( true );
export const showSemantic = writable( false );
