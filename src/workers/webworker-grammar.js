import peg from "metslesliens"

const optionsSemantic = {};
const optionsSyntactic = { partialMatch: true, plugins: [ new peg.SyntacticActionsPlugin( { ignoredRules: [ "entreePartial" ] } ) ] };
let grammar = "", text = "";
let parserSyntactic = { status: null };
let parserSemantic = { status: null };

onconnect = function(e) {
  const port = e.ports[0];

  port.addEventListener('message', function(e) {
    const initial = e.data.initial ? true : false;
    if( e.data.text ) {
      text = e.data.text;
    }
    if( e.data.grammar ) {
      grammar = e.data.grammar;
      try {
        parserSyntactic = peg.computeGrammar( "syntactic", grammar, optionsSyntactic, parserSyntactic );
        if( parserSyntactic.status === false ) {
          port.postMessage(
            {
              grammarStatus: false,
              grammarError: peg.locationError(parserSyntactic.error) + parserSyntactic.error.message
            }
          );
          return;
        }
        parserSemantic = peg.computeGrammar( "semantic", grammar, optionsSemantic, parserSemantic );
        if( parserSemantic.status === false ) {
          port.postMessage(
            {
              grammarStatus: false,
              grammarError: peg.locationError(parserSyntactic.error) + "(semantic) " + parserSyntactic.error.message
            }
          );
          return;
        }
        if( !initial ) {
          port.postMessage(
            {
              grammarStatus: true,
              grammarError: ""
            }
          );
        }
      } catch(e) {
        port.postMessage(
          {
            grammarStatus: false,
            grammarError: e.message
          }
        );
      }
    }
    if( parserSyntactic.status && text ) {
      try {
        const links = peg.getLinks( text, parserSyntactic.parser );
        const linkedText = peg.getLinkedText( text, links );
        if( !initial ) {
          port.postMessage(
            {
              linkedText: linkedText,
              firstLink: links.length ? links[0] : null
            }
          );
        }
        if( parserSemantic.status ) {
          const semanticLinks = peg.getSemanticLinks( links, parserSemantic.parser );
          if( !initial ) {
            port.postMessage(
              {
                semanticLinks: semanticLinks
              }
            );
          }
        }
      } catch(e) {
        port.postMessage(
          {
            grammarStatus: false,
            grammarError: e.message
          }
        );
      }
    }
  });

  port.start();
}

// vim: set ts=2 sw=2 sts=2 et:
